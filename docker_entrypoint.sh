#!/bin/bash

set -ouo pipefail

mkdir -p /root/.ssh
echo ${SSH_PRIVATE_KEY_DATA} | base64 -d > /root/.ssh/id_rsa
chown $(id -u):$(id -g) /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

if [ $# -eq 0 ]; then
  exec ssh -V
else
  exec "$@"
fi