# ssh-client

## Build
```
docker build -t ssh-client .
```

## Configuration

| name | default | description |
| ---- | ------- | ----------- |
| SSH_PRIVATE_KEY_DATA | Cg== | base64 encoded ssh private key |
